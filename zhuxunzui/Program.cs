﻿using System;
using System.Windows.Forms;

namespace zhuxunzui
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] Args)
        {
            string s = "";
            if (Args.Length > 0) s = Args[0];
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new zxz_Main_Form(s));
        }
    }
}
