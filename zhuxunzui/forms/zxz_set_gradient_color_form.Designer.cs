﻿namespace zhuxunzui.forms
{
    partial class zxz_set_gradient_color_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cencel = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.previewPictureBox = new System.Windows.Forms.PictureBox();
            this.choostDircetComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.endColorBtn = new System.Windows.Forms.Button();
            this.startColorBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // cencel
            // 
            this.cencel.Location = new System.Drawing.Point(219, 162);
            this.cencel.Name = "cencel";
            this.cencel.Size = new System.Drawing.Size(75, 28);
            this.cencel.TabIndex = 19;
            this.cencel.Text = "取消";
            this.cencel.UseVisualStyleBackColor = true;
            this.cencel.Click += new System.EventHandler(this.cencel_Click);
            // 
            // confirm
            // 
            this.confirm.Location = new System.Drawing.Point(64, 162);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(75, 28);
            this.confirm.TabIndex = 18;
            this.confirm.Text = "确定";
            this.confirm.UseVisualStyleBackColor = true;
            this.confirm.Click += new System.EventHandler(this.confirm_Click);
            // 
            // previewPictureBox
            // 
            this.previewPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.previewPictureBox.Location = new System.Drawing.Point(230, 92);
            this.previewPictureBox.Name = "previewPictureBox";
            this.previewPictureBox.Size = new System.Drawing.Size(100, 50);
            this.previewPictureBox.TabIndex = 17;
            this.previewPictureBox.TabStop = false;
            // 
            // choostDircetComboBox
            // 
            this.choostDircetComboBox.FormattingEnabled = true;
            this.choostDircetComboBox.Items.AddRange(new object[] {
            "右上左下",
            "左上右下",
            "从左到右",
            "从上到下"});
            this.choostDircetComboBox.Location = new System.Drawing.Point(230, 41);
            this.choostDircetComboBox.Name = "choostDircetComboBox";
            this.choostDircetComboBox.Size = new System.Drawing.Size(80, 20);
            this.choostDircetComboBox.TabIndex = 16;
            this.choostDircetComboBox.Text = "右上左下";
            this.choostDircetComboBox.SelectedIndexChanged += new System.EventHandler(this.choostDircetComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(183, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "预览：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(183, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "方向：";
            // 
            // endColorBtn
            // 
            this.endColorBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.endColorBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.endColorBtn.Location = new System.Drawing.Point(107, 92);
            this.endColorBtn.Name = "endColorBtn";
            this.endColorBtn.Size = new System.Drawing.Size(32, 30);
            this.endColorBtn.TabIndex = 13;
            this.endColorBtn.UseVisualStyleBackColor = false;
            this.endColorBtn.Click += new System.EventHandler(this.endColorBtn_Click);
            // 
            // startColorBtn
            // 
            this.startColorBtn.BackColor = System.Drawing.Color.Red;
            this.startColorBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.startColorBtn.Location = new System.Drawing.Point(107, 35);
            this.startColorBtn.Name = "startColorBtn";
            this.startColorBtn.Size = new System.Drawing.Size(32, 30);
            this.startColorBtn.TabIndex = 12;
            this.startColorBtn.UseVisualStyleBackColor = false;
            this.startColorBtn.Click += new System.EventHandler(this.startColorBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "结束颜色：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "起始颜色：";
            // 
            // zxz_set_gradient_color_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 271);
            this.Controls.Add(this.cencel);
            this.Controls.Add(this.confirm);
            this.Controls.Add(this.previewPictureBox);
            this.Controls.Add(this.choostDircetComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.endColorBtn);
            this.Controls.Add(this.startColorBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "zxz_set_gradient_color_form";
            this.Text = "zxz_set_gradient_color_form";
            this.Load += new System.EventHandler(this.zxz_set_gradient_color_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cencel;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.PictureBox previewPictureBox;
        private System.Windows.Forms.ComboBox choostDircetComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button endColorBtn;
        private System.Windows.Forms.Button startColorBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}