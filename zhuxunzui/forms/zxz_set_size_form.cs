﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace zhuxunzui.forms
{
    public partial class zxz_set_size_form : Form {
        private zxz_Main_Form parentForm;

        public zxz_set_size_form() {
            InitializeComponent();
        }

        private void zxz_set_size_Load(object sender, EventArgs e) {
            parentForm = (zxz_Main_Form)this.Owner;
            currWidthLabel.Text = parentForm.MainPictureBox.Width.ToString();
            currHeightLabel.Text = parentForm.MainPictureBox.Height.ToString();
        }

        private void confirm_Click(object sender, EventArgs e) {
            if (newWidthTB.Text.Equals("") || newHeightTB.Text.Equals("")) {
                MessageBox.Show("高度与宽度不能为空", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int newWidth = int.Parse(newWidthTB.Text.ToString());
            int newHeight = int.Parse(newHeightTB.Text.ToString());

            if(newWidth <= 0 || newHeight <= 0) {
                MessageBox.Show("高度与宽度不能小于等于零, 请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (newHeight != parentForm.MainPictureBox.Image.Height || newWidth != parentForm.MainPictureBox.Image.Width) {
                Bitmap tempImage = new Bitmap(newWidth, newHeight);
                Graphics draw = Graphics.FromImage(tempImage);
                draw.DrawImage(parentForm.MainPictureBox.Image, 0, 0, tempImage.Width, tempImage.Height);
                
                parentForm.MainPictureBox.Image = tempImage;
                parentForm.MainPictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            }

            this.Close();
        }

        private void cencle_Click(object sender, EventArgs e) {
            this.Close();
        }


    }
}
