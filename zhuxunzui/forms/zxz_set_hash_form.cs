﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace zhuxunzui.forms
{
    public partial class zxz_set_hash_form : Form
    {

        private zxz_Main_Form parentForm;
        private HatchStyle[] hs;
        public zxz_set_hash_form()
        {
            InitializeComponent();
            hs = new HatchStyle[] { HatchStyle.BackwardDiagonal, HatchStyle.Cross, HatchStyle.DarkDownwardDiagonal,
                HatchStyle.DarkHorizontal, HatchStyle.DarkUpwardDiagonal, HatchStyle.DarkVertical,
                HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal, HatchStyle.DashedUpwardDiagonal,
                HatchStyle.DashedVertical, HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross, HatchStyle.Divot, 
                HatchStyle.DottedDiamond, HatchStyle.DottedGrid, HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard, HatchStyle.Vertical, HatchStyle.Wave,
                HatchStyle.ZigZag };
        }

        private void zxz_set_hash_form_Load(object sender, EventArgs e)
        {
            this.parentForm = (zxz_Main_Form)this.Owner;

            backColorBT.BackColor = this.parentForm.DrawStyles.HashConfig.BackColor;
            frontColorBT.BackColor = this.parentForm.DrawStyles.HashConfig.FrontColor;

            //为纹理类型下拉列表赋值，并确定选中项
            for (int i = 0; i < hs.Length;i++)
            {
                comboBox1.Items.Add(hs[i].ToString());
                if (this.parentForm.DrawStyles.HashConfig.HashStyle.ToString() == hs[i].ToString()) {
                    comboBox1.SelectedIndex = i;
                }
            }
        }

        //效果预览
        private void preview() {

            Graphics g = CreateGraphics();
            Rectangle rec = new Rectangle(label4.Left+50,label4.Top-20,163,51);
            HatchBrush hb = new HatchBrush(this.parentForm.DrawStyles.HashConfig.HashStyle, 
                this.parentForm.DrawStyles.HashConfig.FrontColor,
                this.parentForm.DrawStyles.HashConfig.BackColor);

            g.FillEllipse(hb,rec);
            g.Dispose();
        }

        /**
         * 
         * 前景色点击
         * **/
        private void frontColorBT_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = true;
            dialog.Color = frontColorBT.BackColor;
            if(dialog.ShowDialog()==DialogResult.OK){
                frontColorBT.BackColor = dialog.Color;
                this.parentForm.DrawStyles.HashConfig.FrontColor = dialog.Color;
                preview();
            }
        }
        /**
         * 背景色
         * **/
        private void backColorBT_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = true;
            dialog.Color = backColorBT.BackColor;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                backColorBT.BackColor = dialog.Color;
                this.parentForm.DrawStyles.HashConfig.BackColor = dialog.Color;
                preview();
            }
        }

        void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.parentForm.DrawStyles.HashConfig.HashStyle=hs[comboBox1.SelectedIndex];
            preview();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /**
         * 确认
         * **/
        private void confirmBtn_Click(object sender, EventArgs e)
        {
            this.parentForm.DrawStyles.ColorTypeF = core.DrawStyles.ColorType.Texture;
            this.Close();
        }

        /**
         * 为解决每次加载完成后 并没有初始的效果预览 用这个事件可以解决
         * 
         * */
        private void zxz_set_hash_form_Shown(object sender, EventArgs e)
        {
            preview();
        }
    }
}
