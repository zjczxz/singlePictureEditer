﻿namespace zhuxunzui.forms {
    partial class zxz_set_size_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cencle = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.newHeightTB = new System.Windows.Forms.TextBox();
            this.newWidthTB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.currHeightLabel = new System.Windows.Forms.Label();
            this.currWidthLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cencle
            // 
            this.cencle.Location = new System.Drawing.Point(322, 261);
            this.cencle.Name = "cencle";
            this.cencle.Size = new System.Drawing.Size(88, 30);
            this.cencle.TabIndex = 7;
            this.cencle.Text = "取消";
            this.cencle.UseVisualStyleBackColor = true;
            this.cencle.Click += new System.EventHandler(this.cencle_Click);
            // 
            // confirm
            // 
            this.confirm.Location = new System.Drawing.Point(101, 261);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(88, 30);
            this.confirm.TabIndex = 6;
            this.confirm.Text = "确定";
            this.confirm.UseVisualStyleBackColor = true;
            this.confirm.Click += new System.EventHandler(this.confirm_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.newHeightTB);
            this.groupBox2.Controls.Add(this.newWidthTB);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(37, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 101);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设置图像像素";
            // 
            // newHeightTB
            // 
            this.newHeightTB.Location = new System.Drawing.Point(305, 50);
            this.newHeightTB.Name = "newHeightTB";
            this.newHeightTB.Size = new System.Drawing.Size(57, 21);
            this.newHeightTB.TabIndex = 9;
            // 
            // newWidthTB
            // 
            this.newWidthTB.Location = new System.Drawing.Point(82, 50);
            this.newWidthTB.Name = "newWidthTB";
            this.newWidthTB.Size = new System.Drawing.Size(57, 21);
            this.newWidthTB.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(377, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 12);
            this.label10.TabIndex = 7;
            this.label10.Text = "pix";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(146, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "pix";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(258, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "高度：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "宽度：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.currHeightLabel);
            this.groupBox1.Controls.Add(this.currWidthLabel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(37, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 100);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "当前大小";
            // 
            // currHeightLabel
            // 
            this.currHeightLabel.AutoSize = true;
            this.currHeightLabel.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.currHeightLabel.Location = new System.Drawing.Point(324, 45);
            this.currHeightLabel.Name = "currHeightLabel";
            this.currHeightLabel.Size = new System.Drawing.Size(23, 12);
            this.currHeightLabel.TabIndex = 5;
            this.currHeightLabel.Text = "388";
            // 
            // currWidthLabel
            // 
            this.currWidthLabel.AutoSize = true;
            this.currWidthLabel.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.currWidthLabel.Location = new System.Drawing.Point(97, 45);
            this.currWidthLabel.Name = "currWidthLabel";
            this.currWidthLabel.Size = new System.Drawing.Size(23, 12);
            this.currWidthLabel.TabIndex = 4;
            this.currWidthLabel.Text = "710";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(377, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "pix";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(146, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "pix";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "高度：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "宽度：";
            // 
            // zxz_set_size_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 334);
            this.Controls.Add(this.cencle);
            this.Controls.Add(this.confirm);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "zxz_set_size_form";
            this.Text = "设置图片大小";
            this.Load += new System.EventHandler(this.zxz_set_size_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cencle;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox newHeightTB;
        private System.Windows.Forms.TextBox newWidthTB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label currHeightLabel;
        private System.Windows.Forms.Label currWidthLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}