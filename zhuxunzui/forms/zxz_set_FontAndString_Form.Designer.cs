﻿namespace zhuxunzui.forms
{
    partial class zxz_set_FontAndString_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lable1 = new System.Windows.Forms.Label();
            this.fontPreView = new System.Windows.Forms.Label();
            this.setFontBTN = new System.Windows.Forms.Button();
            this.confirmBTN = new System.Windows.Forms.Button();
            this.cencleBTN = new System.Windows.Forms.Button();
            this.drawStringTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lable1
            // 
            this.lable1.AutoSize = true;
            this.lable1.Location = new System.Drawing.Point(54, 23);
            this.lable1.Name = "lable1";
            this.lable1.Size = new System.Drawing.Size(65, 12);
            this.lable1.TabIndex = 0;
            this.lable1.Text = "文本内容：";
            // 
            // fontPreView
            // 
            this.fontPreView.AutoSize = true;
            this.fontPreView.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fontPreView.Location = new System.Drawing.Point(152, 109);
            this.fontPreView.Name = "fontPreView";
            this.fontPreView.Size = new System.Drawing.Size(94, 21);
            this.fontPreView.TabIndex = 1;
            this.fontPreView.Text = "字体展示";
            // 
            // setFontBTN
            // 
            this.setFontBTN.Location = new System.Drawing.Point(56, 109);
            this.setFontBTN.Name = "setFontBTN";
            this.setFontBTN.Size = new System.Drawing.Size(75, 23);
            this.setFontBTN.TabIndex = 2;
            this.setFontBTN.Text = "字体";
            this.setFontBTN.UseVisualStyleBackColor = true;
            this.setFontBTN.Click += new System.EventHandler(this.setFontBTN_Click);
            // 
            // confirmBTN
            // 
            this.confirmBTN.Location = new System.Drawing.Point(288, 109);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(75, 23);
            this.confirmBTN.TabIndex = 3;
            this.confirmBTN.Text = "确定";
            this.confirmBTN.UseVisualStyleBackColor = true;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // cencleBTN
            // 
            this.cencleBTN.Location = new System.Drawing.Point(385, 109);
            this.cencleBTN.Name = "cencleBTN";
            this.cencleBTN.Size = new System.Drawing.Size(75, 23);
            this.cencleBTN.TabIndex = 4;
            this.cencleBTN.Text = "取消";
            this.cencleBTN.UseVisualStyleBackColor = true;
            this.cencleBTN.Click += new System.EventHandler(this.cencleBTN_Click);
            // 
            // drawStringTextBox
            // 
            this.drawStringTextBox.Location = new System.Drawing.Point(55, 52);
            this.drawStringTextBox.Name = "drawStringTextBox";
            this.drawStringTextBox.Size = new System.Drawing.Size(373, 21);
            this.drawStringTextBox.TabIndex = 5;
            // 
            // zxz_set_FontAndString_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 171);
            this.Controls.Add(this.drawStringTextBox);
            this.Controls.Add(this.cencleBTN);
            this.Controls.Add(this.confirmBTN);
            this.Controls.Add(this.setFontBTN);
            this.Controls.Add(this.fontPreView);
            this.Controls.Add(this.lable1);
            this.Name = "zxz_set_FontAndString_Form";
            this.Text = "添加文字设置框";
            this.Load += new System.EventHandler(this.zxz_set_FontAndString_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lable1;
        private System.Windows.Forms.Label fontPreView;
        private System.Windows.Forms.Button setFontBTN;
        private System.Windows.Forms.Button confirmBTN;
        private System.Windows.Forms.Button cencleBTN;
        private System.Windows.Forms.TextBox drawStringTextBox;
    }
}