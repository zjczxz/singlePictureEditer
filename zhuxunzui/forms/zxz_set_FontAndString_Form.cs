﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zhuxunzui.forms
{
    public partial class zxz_set_FontAndString_Form : Form
    {

        private zxz_Main_Form parentForm;

        public zxz_set_FontAndString_Form() {
            InitializeComponent();
        }

        private void confirmBTN_Click(object sender, EventArgs e) {
            this.parentForm.DrawStyles.DrawString = this.drawStringTextBox.Text;
            if (this.drawStringTextBox.Text.Equals("")) {
                MessageBox.Show("文本内容不能为空", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.Close();
        }

        private void cencleBTN_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void setFontBTN_Click(object sender, EventArgs e) {
            FontDialog fontDialog = new FontDialog();
            fontDialog.ShowColor = false;
            fontDialog.Font = this.parentForm.DrawStyles.Font;
            if (fontDialog.ShowDialog() == DialogResult.OK) {
                this.parentForm.DrawStyles.Font = fontDialog.Font;
                fontPreView.Font = this.parentForm.DrawStyles.Font;
            }
        }

        private void zxz_set_FontAndString_Form_Load(object sender, EventArgs e) {
            this.parentForm = (zxz_Main_Form)this.Owner;
            fontPreView.Font = this.parentForm.DrawStyles.Font;
        }
    }
}
