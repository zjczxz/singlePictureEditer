﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace zhuxunzui.forms
{
    public partial class zxz_set_gradient_color_form : Form
    {

        private zxz_Main_Form parentForm;

        public zxz_set_gradient_color_form() {
            InitializeComponent();
        }

        private void startColorBtn_Click(object sender, EventArgs e) {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = true;
            dialog.Color = startColorBtn.BackColor;
            if (dialog.ShowDialog() == DialogResult.OK) {
                startColorBtn.BackColor = dialog.Color;
                this.parentForm.DrawStyles.GradientConfig.StartColor = dialog.Color;

                updatePreviewPictureBox();
            }
        }

        private void endColorBtn_Click(object sender, EventArgs e) {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = true;
            dialog.Color = endColorBtn.BackColor;
            if (dialog.ShowDialog() == DialogResult.OK) {
                endColorBtn.BackColor = dialog.Color;
                this.parentForm.DrawStyles.GradientConfig.EndColor = dialog.Color;
                updatePreviewPictureBox();
            }
        }

        private void choostDircetComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            int selectIdex = this.choostDircetComboBox.SelectedIndex;
            switch (selectIdex) {
                case 0: this.parentForm.DrawStyles.GradientConfig.Dir = LinearGradientMode.BackwardDiagonal;
                    break;
                case 1: this.parentForm.DrawStyles.GradientConfig.Dir = LinearGradientMode.ForwardDiagonal;
                    break;
                case 2: this.parentForm.DrawStyles.GradientConfig.Dir = LinearGradientMode.Horizontal;
                    break;
                case 3: this.parentForm.DrawStyles.GradientConfig.Dir = LinearGradientMode.Vertical;
                    break;
            }
            this.updatePreviewPictureBox();
        }

        private void confirm_Click(object sender, EventArgs e) {
            this.parentForm.DrawStyles.ColorTypeF = core.DrawStyles.ColorType.GradientColor;
            this.Close();
        }

        private void cencel_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void updatePreviewPictureBox() {
            LinearGradientBrush brush = null;
            brush = new LinearGradientBrush(
                new Rectangle(0, 0, this.previewPictureBox.Width, this.previewPictureBox.Height),
                this.parentForm.DrawStyles.GradientConfig.StartColor,
                this.parentForm.DrawStyles.GradientConfig.EndColor,
                this.parentForm.DrawStyles.GradientConfig.Dir);
            
            Bitmap newimg = new Bitmap(this.previewPictureBox.Width, this.previewPictureBox.Height);
            Graphics g = Graphics.FromImage(newimg);
            g.FillRectangle(brush, new Rectangle(0, 0,
                this.previewPictureBox.Width,
                this.previewPictureBox.Height));

            this.previewPictureBox.Image = newimg;
            brush.Dispose();
        }

        private void zxz_set_gradient_color_form_Load(object sender, EventArgs e) {
            this.parentForm = (zxz_Main_Form)this.Owner;
            this.endColorBtn.BackColor = this.parentForm.DrawStyles.GradientConfig.EndColor;
            this.startColorBtn.BackColor = this.parentForm.DrawStyles.GradientConfig.StartColor;
            Bitmap newimg = new Bitmap(this.previewPictureBox.Width, this.previewPictureBox.Height);
            
            this.choostDircetComboBox.SelectedIndex = 3;

        }
    }
}
