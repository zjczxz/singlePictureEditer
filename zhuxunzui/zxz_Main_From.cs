﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using zhuxunzui.core;

namespace zhuxunzui
{
    public partial class zxz_Main_Form : Form
    {
        private imgControler imgClr;

        internal imgControler ImgClr {
            get { return imgClr; }
            set { imgClr = value; }
        }

        private zhuxunzui.core.DrawStyles drawStyles;

        internal zhuxunzui.core.DrawStyles DrawStyles {
            get { return drawStyles; }
            set { drawStyles = value; }
        }

        //图片编辑父类 在选择不同类型的曲线时使用多态
        private zhuxunzui.core.DrawPicture drawPicture;

        public zxz_Main_Form()
        {
            InitializeComponent();
        }
        private String fileName;
        public zxz_Main_Form(string s)
        {
            InitializeComponent();
            this.fileName = s;
        }

        /*
         * 窗口加载时的一些初始化工作 待定
         * */
        private void Form1_Load(object sender, EventArgs e)
        {


            this.imgClr = new imgControler(mainPictureBox);
            this.drawStyles = new DrawStyles();
            this.drawStyles.Color = Color.Black;
            this.color_show.BackColor = Color.Black;

            for (int i = 1; i < 6; i++)
                LineWidthtoolStripComboBox.Items.Add(i);
            LineWidthtoolStripComboBox.SelectedIndex = 0;
            this.radianTextBox.Text = this.DrawStyles.Radian.ToString();
            if (!this.fileName.Equals(""))
            {
                imgClr.openImg(this.fileName);
            }
            mainPictureBox.SendToBack();
            //originPosition = new Point(-999999,-999999);
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.imgClr.openImg())
            {
                //open success;
            }
            else
            {
                //open faild;
            }

        }

        private void 新建ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.imgClr.newImg())
            {
                //create success
            }
            else
            {
                // create faild
            }
        }

        private void 另存为ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.imgClr.saveAs())
            {
                //saveas success
            }
            else
            {
                // saveAs faild
            }
        }

        /**
         * 退出时 释放一些资源
         * */
        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.imgClr.saveImg())
            {
                //create success
            }
            else
            {
                // create faild
            }
        }

        private void 实际大小ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.imgClr.orignSize();
        }

        private void 放大ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.imgClr.enlarge();
        }

        private void 缩小ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.imgClr.reduce();
        }

        private void 水平翻转ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imgClr.convertImage(imgControler.ConvertType.Rotate180FlipY);
        }

        private void 垂直翻转ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imgClr.convertImage(imgControler.ConvertType.Rotate180FlipX);
        }

        private void 顺时针旋转90度ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imgClr.convertImage(imgControler.ConvertType.Rotate90FlipNone);
        }

        private void 逆时针旋转90度ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imgClr.convertImage(imgControler.ConvertType.Rotate270FlipNone);
        }

        private void 反色ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                           zhuxunzui.core.ImgEffect.EffectType.Reverse);
            this.imgClr.Modify = true;
        }

        private void 浮雕ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Relief);
            this.imgClr.Modify = true;
        }

        private void 黑白ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.BlackWhite);
            this.imgClr.Modify = true;
        }

        private void 柔化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Soften);
            this.imgClr.Modify = true;
        }

        private void 锐化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Sharpening);
            this.imgClr.Modify = true;
        }

        private void 灰度化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Gray);
            this.imgClr.Modify = true;
        }

        private void 雾化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Atomization);
            this.imgClr.Modify = true;
        }

        private void 马赛克效果ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPictureBox.Image = zhuxunzui.core.ImgEffect.setEffect((Bitmap)mainPictureBox.Image,
                            zhuxunzui.core.ImgEffect.EffectType.Mosaic);
            this.imgClr.Modify = true;
        }

        private void 图像大小设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mainPictureBox.Image == null)
            {
                MessageBox.Show("未打开任何图片", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            forms.zxz_set_size_form set_size_form = new forms.zxz_set_size_form();
            set_size_form.Owner = this;
            set_size_form.ShowDialog();
            this.imgClr.Modify = true;
        }

        private void 纯色ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = true;
            dialog.Color = this.color_show.BackColor;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                drawStyles.ColorTypeF = zhuxunzui.core.DrawStyles.ColorType.SingleColor;
                drawStyles.Color = dialog.Color;
                this.color_show.BackColor = dialog.Color;
                this.color_show.Text = ""; //因为有其他一些模式 不能总用color来表示，所以需要文字
            }
        }

        private void 渐变色ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            forms.zxz_set_gradient_color_form set_gradient_color_form = new forms.zxz_set_gradient_color_form();
            set_gradient_color_form.Owner = this;
            set_gradient_color_form.ShowDialog();
        }

        private void 纹理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            forms.zxz_set_hash_form set_hash_form = new forms.zxz_set_hash_form();
            set_hash_form.Owner = this;
            set_hash_form.ShowDialog();
        }

        private void 图片填充ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "bmp,jpg,gif,png,tiff,icon|*.bmp;*.jpg;*.gif;*.png;*.tiff;*.icon";
            dialog.Title = "选择图片";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.drawStyles.FillImg = (Bitmap)Image.FromFile(dialog.FileName);
                this.drawStyles.ColorTypeF = core.DrawStyles.ColorType.PictureFill;
            }
        }

        private void mainPictureBox_SizeChanged(object sender, EventArgs e)
        {

        }

        private void chooseButton_Click(object sender, EventArgs e)
        {
            this.drawPicture = null;
            this.Cursor = Cursors.Default;
        }

        private void line_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.SingleLine;
            drawPicture = new zhuxunzui.core.drawRelative.DrawLine(drawStyles);
        }

        private void curve_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.Curve;
            drawPicture = new zhuxunzui.core.drawRelative.DrawCurve(drawStyles);
        }

        private void curvePlus_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.CurvePlus;
            drawPicture = new zhuxunzui.core.drawRelative.DrawArc(drawStyles);
        }

        private void rectangle_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.Rectangle;
            drawPicture = new zhuxunzui.core.drawRelative.DrawRectangle(drawStyles);
        }

        private void solidRectangle_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.SolidRectangle;
            drawPicture = new zhuxunzui.core.drawRelative.DrawRectangle(drawStyles);
        }

        private void circle_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.Circle;
            drawPicture = new zhuxunzui.core.drawRelative.DrawCircle(drawStyles);
        }

        private void solidCircle_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.SolidCircle;
            drawPicture = new zhuxunzui.core.drawRelative.DrawCircle(drawStyles);
        }

        private void polygon_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.Polygon;
            drawPicture = new zhuxunzui.core.drawRelative.DrawPolygon(drawStyles);
        }

        private void solidPolygon_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.SolidPolygon;
            drawPicture = new zhuxunzui.core.drawRelative.DrawPolygon(drawStyles);
        }

        private void addText_Click(object sender, EventArgs e)
        {
            zhuxunzui.forms.zxz_set_FontAndString_Form set_FontAndString_Form = new forms.zxz_set_FontAndString_Form();
            set_FontAndString_Form.Owner = this;
            set_FontAndString_Form.ShowDialog();
            this.Cursor = Cursors.Cross;
            drawStyles.DrawTypeF = core.DrawStyles.DrawType.SolidPolygon;
            drawPicture = new zhuxunzui.core.drawRelative.DrawString(drawStyles);
        }

        //鼠标移动同时画
        private void mainPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawPicture == null)
            {
                
                return;
            }

            drawPicture.mouseMove(mainPictureBox, e);
        }


        //结束绘画
        private void mainPictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (drawPicture == null)
                return;

            this.Cursor = Cursors.Default;
            drawPicture.mouseDoubleClick(mainPictureBox, e);
            drawPicture = null;
        }

        //开始画线
        private void mainPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (drawPicture == null)
            {
              
                return;
            }
            drawPicture.mouseDown(mainPictureBox, e);
            this.imgClr.Modify = true;
        }

        //鼠标抬起 停止画线
        private void mainPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (drawPicture == null)
            {
                
                return;
            }
            if (drawPicture.mouseUp(mainPictureBox, e))
                mainPictureBox_MouseDoubleClick(sender, e);
        }

        private void LineStyleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LineType_toolStripDropDownButton1.Image = ((ToolStripMenuItem)sender).Image;

            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            string itemName = item.Name;
            itemName = itemName.Substring(0, itemName.IndexOf('_'));
            Console.WriteLine("{0}", itemName);
            switch (itemName)
            {
                case "dssh":
                    {//dash
                        drawStyles.DashStyle = DashStyle.Dash;
                        break;
                    }
                case "dashDot":
                    {//DashDot
                        drawStyles.DashStyle = DashStyle.DashDot;
                        break;
                    }
                case "dashDotDot":
                    {//dashdotdot
                        drawStyles.DashStyle = DashStyle.DashDotDot;
                        break;
                    }
                case "solid":
                    {//Solid
                        drawStyles.DashStyle = DashStyle.Solid;
                        break;
                    }
                case "dot":
                    {
                        drawStyles.DashStyle = DashStyle.Dot;
                        break;
                    }
                default:
                    {
                        drawStyles.DashStyle = DashStyle.Solid;
                        break;
                    }
            }
        }

        private void SerLineCapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            string currentName = item.Name;
            switch (currentName)
            {
                case "arrowAnchorToolStripMenuItem":
                    {//ArrowAnchor
                        drawStyles.StartLineCap = LineCap.ArrowAnchor;
                        break;
                    }
                case "diamondAnchorToolStripMenuItem":
                    {
                        drawStyles.StartLineCap = LineCap.DiamondAnchor;
                        break;
                    }
                case "squareAnchorToolStripMenuItem":
                    {
                        drawStyles.StartLineCap = LineCap.SquareAnchor;
                        break;
                    }
                case "triangleToolStripMenuItem":
                    {
                        drawStyles.StartLineCap = LineCap.Triangle;
                        break;
                    }
                case "roundAnchorToolStripMenuItem":
                    {
                        drawStyles.StartLineCap = LineCap.RoundAnchor;
                        break;
                    }
                case "noanchorToolStripMenuItem":
                    {
                        drawStyles.StartLineCap = LineCap.NoAnchor;
                        break;
                    }
                default:
                    { //考虑和上面一条合并 有待商榷
                        drawStyles.StartLineCap = LineCap.NoAnchor;
                        break;
                    }
            }
        }

        private void setEndLineCapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            string currentName = item.Name;
            switch (currentName)
            {
                case "arrowAnchorToolStripMenuItem1":
                    {//ArrowAnchor
                        drawStyles.EndLineCap = LineCap.ArrowAnchor;
                        break;
                    }
                case "diamondAnchorToolStripMenuItem1":
                    {
                        drawStyles.EndLineCap = LineCap.DiamondAnchor;
                        break;
                    }
                case "squareAnchorToolStripMenuItem1":
                    {
                        drawStyles.EndLineCap = LineCap.SquareAnchor;
                        break;
                    }
                case "triangleToolStripMenuItem1":
                    {
                        drawStyles.EndLineCap = LineCap.Triangle;
                        break;
                    }
                case "roundAnchorToolStripMenuItem1":
                    {
                        drawStyles.EndLineCap = LineCap.RoundAnchor;
                        break;
                    }
                case "noanchorToolStripMenuItem1":
                    {
                        drawStyles.EndLineCap = LineCap.NoAnchor;
                        break;
                    }
                default:
                    { //考虑和上面一条合并 有待商榷
                        MessageBox.Show("结束端点未匹配", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        drawStyles.StartLineCap = LineCap.NoAnchor;
                        break;
                    }
            }
        }

        private void LineWidthtoolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            drawStyles.LineWidth = int.Parse(LineWidthtoolStripComboBox.SelectedItem.ToString());
        }

        private void mainPictureBox_Click(object sender, EventArgs e)
        {
            if (drawPicture == null)
                return;
            drawPicture.mouseClick(mainPictureBox, (MouseEventArgs)e);
        }

        private void zxz_Main_Form_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void zxz_Main_Form_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            int i;
            for (i = 0; i < s.Length; i++)
            {
                if (s[i].Trim() != "")
                {
                    this.imgClr.openImg(s[i]);
                }

            }
        }

        private void radianTextBox_TextChanged(object sender, EventArgs e)
        {
            float radian = float.Parse(this.radianTextBox.Text.ToString());
            if (radian <= 0 || radian > 180)
            {
                MessageBox.Show("弧度不能大于180", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.radianTextBox.Text = this.DrawStyles.Radian.ToString();
                return;
            }
            this.DrawStyles.Radian = float.Parse(this.radianTextBox.Text.ToString());
        }

        private void zxz_Main_Form_Resize(object sender, EventArgs e)
        {

        }
    }
}
