﻿namespace zhuxunzui
{
    partial class zxz_Main_Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(zxz_Main_Form));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新建ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.另存为ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.实际大小ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.放大ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩小ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.水平翻转ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.垂直翻转ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.顺时针旋转90度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.逆时针旋转90度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.反色ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.浮雕ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.黑白ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.柔化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锐化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.灰度化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.雾化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.马赛克效果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像大小设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.绘图设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.线型ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dssh__ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dash_Dot_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashDotDot_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solid_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.起点终点ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrowAnchorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diamondAnchorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.squareAnchorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roundAnchorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noanchorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.终点端点ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrowAnchorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.diamondAnchorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.squareAnchorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.triangleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.roundAnchorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.noanchorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.颜色设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.纯色ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.渐变色ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.纹理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片填充ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.快捷按钮 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.singleColorChoose = new System.Windows.Forms.ToolStripButton();
            this.color_show = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.lineWidth = new System.Windows.Forms.ToolStripLabel();
            this.LineWidthtoolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.lineType = new System.Windows.Forms.ToolStripLabel();
            this.LineType_toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.dssh_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashDot_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashDotDot_ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.solid_ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dot_ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.radianTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.chooseButton = new System.Windows.Forms.ToolStripButton();
            this.line = new System.Windows.Forms.ToolStripButton();
            this.curve = new System.Windows.Forms.ToolStripButton();
            this.curvePlus = new System.Windows.Forms.ToolStripButton();
            this.rectangle = new System.Windows.Forms.ToolStripButton();
            this.solidRectangle = new System.Windows.Forms.ToolStripButton();
            this.circle = new System.Windows.Forms.ToolStripButton();
            this.solidCircle = new System.Windows.Forms.ToolStripButton();
            this.polygon = new System.Windows.Forms.ToolStripButton();
            this.solidPolygon = new System.Windows.Forms.ToolStripButton();
            this.addText = new System.Windows.Forms.ToolStripButton();
            this.mainPictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.快捷按钮.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.查看ToolStripMenuItem,
            this.图片编辑ToolStripMenuItem,
            this.绘图设置ToolStripMenuItem,
            this.颜色设置ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(909, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem,
            this.新建ToolStripMenuItem,
            this.保存ToolStripMenuItem,
            this.另存为ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // 新建ToolStripMenuItem
            // 
            this.新建ToolStripMenuItem.Name = "新建ToolStripMenuItem";
            this.新建ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.新建ToolStripMenuItem.Text = "新建";
            this.新建ToolStripMenuItem.Click += new System.EventHandler(this.新建ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // 另存为ToolStripMenuItem
            // 
            this.另存为ToolStripMenuItem.Name = "另存为ToolStripMenuItem";
            this.另存为ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.另存为ToolStripMenuItem.Text = "另存为";
            this.另存为ToolStripMenuItem.Click += new System.EventHandler(this.另存为ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 查看ToolStripMenuItem
            // 
            this.查看ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.实际大小ToolStripMenuItem,
            this.放大ToolStripMenuItem,
            this.缩小ToolStripMenuItem});
            this.查看ToolStripMenuItem.Name = "查看ToolStripMenuItem";
            this.查看ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.查看ToolStripMenuItem.Text = "查看";
            // 
            // 实际大小ToolStripMenuItem
            // 
            this.实际大小ToolStripMenuItem.Name = "实际大小ToolStripMenuItem";
            this.实际大小ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.实际大小ToolStripMenuItem.Text = "实际大小";
            this.实际大小ToolStripMenuItem.Click += new System.EventHandler(this.实际大小ToolStripMenuItem_Click);
            // 
            // 放大ToolStripMenuItem
            // 
            this.放大ToolStripMenuItem.Name = "放大ToolStripMenuItem";
            this.放大ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.放大ToolStripMenuItem.Text = "放大";
            this.放大ToolStripMenuItem.Click += new System.EventHandler(this.放大ToolStripMenuItem_Click);
            // 
            // 缩小ToolStripMenuItem
            // 
            this.缩小ToolStripMenuItem.Name = "缩小ToolStripMenuItem";
            this.缩小ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.缩小ToolStripMenuItem.Text = "缩小";
            this.缩小ToolStripMenuItem.Click += new System.EventHandler(this.缩小ToolStripMenuItem_Click);
            // 
            // 图片编辑ToolStripMenuItem
            // 
            this.图片编辑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.水平翻转ToolStripMenuItem,
            this.垂直翻转ToolStripMenuItem,
            this.顺时针旋转90度ToolStripMenuItem,
            this.逆时针旋转90度ToolStripMenuItem,
            this.反色ToolStripMenuItem,
            this.浮雕ToolStripMenuItem,
            this.黑白ToolStripMenuItem,
            this.柔化ToolStripMenuItem,
            this.锐化ToolStripMenuItem,
            this.灰度化ToolStripMenuItem,
            this.雾化ToolStripMenuItem,
            this.马赛克效果ToolStripMenuItem,
            this.图像大小设置ToolStripMenuItem});
            this.图片编辑ToolStripMenuItem.Name = "图片编辑ToolStripMenuItem";
            this.图片编辑ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.图片编辑ToolStripMenuItem.Text = "图片编辑";
            // 
            // 水平翻转ToolStripMenuItem
            // 
            this.水平翻转ToolStripMenuItem.Name = "水平翻转ToolStripMenuItem";
            this.水平翻转ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.水平翻转ToolStripMenuItem.Text = "水平翻转";
            this.水平翻转ToolStripMenuItem.Click += new System.EventHandler(this.水平翻转ToolStripMenuItem_Click);
            // 
            // 垂直翻转ToolStripMenuItem
            // 
            this.垂直翻转ToolStripMenuItem.Name = "垂直翻转ToolStripMenuItem";
            this.垂直翻转ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.垂直翻转ToolStripMenuItem.Text = "垂直翻转";
            this.垂直翻转ToolStripMenuItem.Click += new System.EventHandler(this.垂直翻转ToolStripMenuItem_Click);
            // 
            // 顺时针旋转90度ToolStripMenuItem
            // 
            this.顺时针旋转90度ToolStripMenuItem.Name = "顺时针旋转90度ToolStripMenuItem";
            this.顺时针旋转90度ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.顺时针旋转90度ToolStripMenuItem.Text = "顺时针旋转90度";
            this.顺时针旋转90度ToolStripMenuItem.Click += new System.EventHandler(this.顺时针旋转90度ToolStripMenuItem_Click);
            // 
            // 逆时针旋转90度ToolStripMenuItem
            // 
            this.逆时针旋转90度ToolStripMenuItem.Name = "逆时针旋转90度ToolStripMenuItem";
            this.逆时针旋转90度ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.逆时针旋转90度ToolStripMenuItem.Text = "逆时针旋转90度";
            this.逆时针旋转90度ToolStripMenuItem.Click += new System.EventHandler(this.逆时针旋转90度ToolStripMenuItem_Click);
            // 
            // 反色ToolStripMenuItem
            // 
            this.反色ToolStripMenuItem.Name = "反色ToolStripMenuItem";
            this.反色ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.反色ToolStripMenuItem.Text = "反色";
            this.反色ToolStripMenuItem.Click += new System.EventHandler(this.反色ToolStripMenuItem_Click);
            // 
            // 浮雕ToolStripMenuItem
            // 
            this.浮雕ToolStripMenuItem.Name = "浮雕ToolStripMenuItem";
            this.浮雕ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.浮雕ToolStripMenuItem.Text = "浮雕";
            this.浮雕ToolStripMenuItem.Click += new System.EventHandler(this.浮雕ToolStripMenuItem_Click);
            // 
            // 黑白ToolStripMenuItem
            // 
            this.黑白ToolStripMenuItem.Name = "黑白ToolStripMenuItem";
            this.黑白ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.黑白ToolStripMenuItem.Text = "黑白";
            this.黑白ToolStripMenuItem.Click += new System.EventHandler(this.黑白ToolStripMenuItem_Click);
            // 
            // 柔化ToolStripMenuItem
            // 
            this.柔化ToolStripMenuItem.Name = "柔化ToolStripMenuItem";
            this.柔化ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.柔化ToolStripMenuItem.Text = "柔化";
            this.柔化ToolStripMenuItem.Click += new System.EventHandler(this.柔化ToolStripMenuItem_Click);
            // 
            // 锐化ToolStripMenuItem
            // 
            this.锐化ToolStripMenuItem.Name = "锐化ToolStripMenuItem";
            this.锐化ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.锐化ToolStripMenuItem.Text = "锐化";
            this.锐化ToolStripMenuItem.Click += new System.EventHandler(this.锐化ToolStripMenuItem_Click);
            // 
            // 灰度化ToolStripMenuItem
            // 
            this.灰度化ToolStripMenuItem.Name = "灰度化ToolStripMenuItem";
            this.灰度化ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.灰度化ToolStripMenuItem.Text = "灰度化";
            this.灰度化ToolStripMenuItem.Click += new System.EventHandler(this.灰度化ToolStripMenuItem_Click);
            // 
            // 雾化ToolStripMenuItem
            // 
            this.雾化ToolStripMenuItem.Name = "雾化ToolStripMenuItem";
            this.雾化ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.雾化ToolStripMenuItem.Text = "雾化";
            this.雾化ToolStripMenuItem.Click += new System.EventHandler(this.雾化ToolStripMenuItem_Click);
            // 
            // 马赛克效果ToolStripMenuItem
            // 
            this.马赛克效果ToolStripMenuItem.Name = "马赛克效果ToolStripMenuItem";
            this.马赛克效果ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.马赛克效果ToolStripMenuItem.Text = "马赛克效果";
            this.马赛克效果ToolStripMenuItem.Click += new System.EventHandler(this.马赛克效果ToolStripMenuItem_Click);
            // 
            // 图像大小设置ToolStripMenuItem
            // 
            this.图像大小设置ToolStripMenuItem.Name = "图像大小设置ToolStripMenuItem";
            this.图像大小设置ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.图像大小设置ToolStripMenuItem.Text = "图像大小设置";
            this.图像大小设置ToolStripMenuItem.Click += new System.EventHandler(this.图像大小设置ToolStripMenuItem_Click);
            // 
            // 绘图设置ToolStripMenuItem
            // 
            this.绘图设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.线型ToolStripMenuItem,
            this.起点终点ToolStripMenuItem,
            this.终点端点ToolStripMenuItem});
            this.绘图设置ToolStripMenuItem.Name = "绘图设置ToolStripMenuItem";
            this.绘图设置ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.绘图设置ToolStripMenuItem.Text = "绘图设置";
            // 
            // 线型ToolStripMenuItem
            // 
            this.线型ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dssh__ToolStripMenuItem,
            this.dash_Dot_ToolStripMenuItem,
            this.dashDotDot_ToolStripMenuItem,
            this.solid_ToolStripMenuItem,
            this.dot_ToolStripMenuItem});
            this.线型ToolStripMenuItem.Name = "线型ToolStripMenuItem";
            this.线型ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.线型ToolStripMenuItem.Text = "线型";
            // 
            // dssh__ToolStripMenuItem
            // 
            this.dssh__ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dssh__ToolStripMenuItem.Image")));
            this.dssh__ToolStripMenuItem.Name = "dssh__ToolStripMenuItem";
            this.dssh__ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dssh__ToolStripMenuItem.Text = "Dash";
            this.dssh__ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dash_Dot_ToolStripMenuItem
            // 
            this.dash_Dot_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dash_Dot_ToolStripMenuItem.Image")));
            this.dash_Dot_ToolStripMenuItem.Name = "dash_Dot_ToolStripMenuItem";
            this.dash_Dot_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dash_Dot_ToolStripMenuItem.Text = "DashDot";
            this.dash_Dot_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dashDotDot_ToolStripMenuItem
            // 
            this.dashDotDot_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dashDotDot_ToolStripMenuItem.Image")));
            this.dashDotDot_ToolStripMenuItem.Name = "dashDotDot_ToolStripMenuItem";
            this.dashDotDot_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dashDotDot_ToolStripMenuItem.Text = "DashDotDot";
            this.dashDotDot_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // solid_ToolStripMenuItem
            // 
            this.solid_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("solid_ToolStripMenuItem.Image")));
            this.solid_ToolStripMenuItem.Name = "solid_ToolStripMenuItem";
            this.solid_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.solid_ToolStripMenuItem.Text = "Solid";
            this.solid_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dot_ToolStripMenuItem
            // 
            this.dot_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dot_ToolStripMenuItem.Image")));
            this.dot_ToolStripMenuItem.Name = "dot_ToolStripMenuItem";
            this.dot_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dot_ToolStripMenuItem.Text = "Dot";
            this.dot_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // 起点终点ToolStripMenuItem
            // 
            this.起点终点ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arrowAnchorToolStripMenuItem,
            this.diamondAnchorToolStripMenuItem,
            this.squareAnchorToolStripMenuItem,
            this.triangleToolStripMenuItem,
            this.roundAnchorToolStripMenuItem,
            this.noanchorToolStripMenuItem});
            this.起点终点ToolStripMenuItem.Name = "起点终点ToolStripMenuItem";
            this.起点终点ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.起点终点ToolStripMenuItem.Text = "起点端点";
            // 
            // arrowAnchorToolStripMenuItem
            // 
            this.arrowAnchorToolStripMenuItem.Name = "arrowAnchorToolStripMenuItem";
            this.arrowAnchorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.arrowAnchorToolStripMenuItem.Text = "ArrowAnchor";
            this.arrowAnchorToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // diamondAnchorToolStripMenuItem
            // 
            this.diamondAnchorToolStripMenuItem.Name = "diamondAnchorToolStripMenuItem";
            this.diamondAnchorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.diamondAnchorToolStripMenuItem.Text = "DiamondAnchor";
            this.diamondAnchorToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // squareAnchorToolStripMenuItem
            // 
            this.squareAnchorToolStripMenuItem.Name = "squareAnchorToolStripMenuItem";
            this.squareAnchorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.squareAnchorToolStripMenuItem.Text = "SquareAnchor";
            this.squareAnchorToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // triangleToolStripMenuItem
            // 
            this.triangleToolStripMenuItem.Name = "triangleToolStripMenuItem";
            this.triangleToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.triangleToolStripMenuItem.Text = "Triangle";
            this.triangleToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // roundAnchorToolStripMenuItem
            // 
            this.roundAnchorToolStripMenuItem.Name = "roundAnchorToolStripMenuItem";
            this.roundAnchorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.roundAnchorToolStripMenuItem.Text = "RoundAnchor";
            this.roundAnchorToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // noanchorToolStripMenuItem
            // 
            this.noanchorToolStripMenuItem.Name = "noanchorToolStripMenuItem";
            this.noanchorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.noanchorToolStripMenuItem.Text = "noanchor";
            this.noanchorToolStripMenuItem.Click += new System.EventHandler(this.SerLineCapToolStripMenuItem_Click);
            // 
            // 终点端点ToolStripMenuItem
            // 
            this.终点端点ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arrowAnchorToolStripMenuItem1,
            this.diamondAnchorToolStripMenuItem1,
            this.squareAnchorToolStripMenuItem1,
            this.triangleToolStripMenuItem1,
            this.roundAnchorToolStripMenuItem1,
            this.noanchorToolStripMenuItem1});
            this.终点端点ToolStripMenuItem.Name = "终点端点ToolStripMenuItem";
            this.终点端点ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.终点端点ToolStripMenuItem.Text = "终点端点";
            // 
            // arrowAnchorToolStripMenuItem1
            // 
            this.arrowAnchorToolStripMenuItem1.Name = "arrowAnchorToolStripMenuItem1";
            this.arrowAnchorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.arrowAnchorToolStripMenuItem1.Text = "ArrowAnchor";
            this.arrowAnchorToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // diamondAnchorToolStripMenuItem1
            // 
            this.diamondAnchorToolStripMenuItem1.Name = "diamondAnchorToolStripMenuItem1";
            this.diamondAnchorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.diamondAnchorToolStripMenuItem1.Text = "DiamondAnchor";
            this.diamondAnchorToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // squareAnchorToolStripMenuItem1
            // 
            this.squareAnchorToolStripMenuItem1.Name = "squareAnchorToolStripMenuItem1";
            this.squareAnchorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.squareAnchorToolStripMenuItem1.Text = "SquareAnchor";
            this.squareAnchorToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // triangleToolStripMenuItem1
            // 
            this.triangleToolStripMenuItem1.Name = "triangleToolStripMenuItem1";
            this.triangleToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.triangleToolStripMenuItem1.Text = "Triangle";
            this.triangleToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // roundAnchorToolStripMenuItem1
            // 
            this.roundAnchorToolStripMenuItem1.Name = "roundAnchorToolStripMenuItem1";
            this.roundAnchorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.roundAnchorToolStripMenuItem1.Text = "RoundAnchor";
            this.roundAnchorToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // noanchorToolStripMenuItem1
            // 
            this.noanchorToolStripMenuItem1.Name = "noanchorToolStripMenuItem1";
            this.noanchorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.noanchorToolStripMenuItem1.Text = "noanchor";
            this.noanchorToolStripMenuItem1.Click += new System.EventHandler(this.setEndLineCapToolStripMenuItem_Click);
            // 
            // 颜色设置ToolStripMenuItem
            // 
            this.颜色设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.纯色ToolStripMenuItem,
            this.渐变色ToolStripMenuItem,
            this.纹理ToolStripMenuItem,
            this.图片填充ToolStripMenuItem});
            this.颜色设置ToolStripMenuItem.Name = "颜色设置ToolStripMenuItem";
            this.颜色设置ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.颜色设置ToolStripMenuItem.Text = "颜色设置";
            // 
            // 纯色ToolStripMenuItem
            // 
            this.纯色ToolStripMenuItem.Name = "纯色ToolStripMenuItem";
            this.纯色ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.纯色ToolStripMenuItem.Text = "纯色";
            this.纯色ToolStripMenuItem.Click += new System.EventHandler(this.纯色ToolStripMenuItem_Click);
            // 
            // 渐变色ToolStripMenuItem
            // 
            this.渐变色ToolStripMenuItem.Name = "渐变色ToolStripMenuItem";
            this.渐变色ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.渐变色ToolStripMenuItem.Text = "渐变色";
            this.渐变色ToolStripMenuItem.Click += new System.EventHandler(this.渐变色ToolStripMenuItem_Click);
            // 
            // 纹理ToolStripMenuItem
            // 
            this.纹理ToolStripMenuItem.Name = "纹理ToolStripMenuItem";
            this.纹理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.纹理ToolStripMenuItem.Text = "纹理";
            this.纹理ToolStripMenuItem.Click += new System.EventHandler(this.纹理ToolStripMenuItem_Click);
            // 
            // 图片填充ToolStripMenuItem
            // 
            this.图片填充ToolStripMenuItem.Name = "图片填充ToolStripMenuItem";
            this.图片填充ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.图片填充ToolStripMenuItem.Text = "图片填充";
            this.图片填充ToolStripMenuItem.Click += new System.EventHandler(this.图片填充ToolStripMenuItem_Click);
            // 
            // 快捷按钮
            // 
            this.快捷按钮.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator2,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton9,
            this.toolStripButton10,
            this.toolStripSeparator3,
            this.toolStripButton11,
            this.toolStripButton12,
            this.toolStripButton13,
            this.toolStripButton14,
            this.toolStripSeparator4,
            this.toolStripSeparator5,
            this.singleColorChoose,
            this.color_show,
            this.toolStripSeparator6,
            this.lineWidth,
            this.LineWidthtoolStripComboBox,
            this.toolStripSeparator7,
            this.lineType,
            this.LineType_toolStripDropDownButton1,
            this.ToolStripLabel1,
            this.radianTextBox});
            this.快捷按钮.Location = new System.Drawing.Point(0, 25);
            this.快捷按钮.Name = "快捷按钮";
            this.快捷按钮.Size = new System.Drawing.Size(909, 25);
            this.快捷按钮.TabIndex = 1;
            this.快捷按钮.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "新建";
            this.toolStripButton1.Click += new System.EventHandler(this.新建ToolStripMenuItem_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "打开";
            this.toolStripButton2.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "保存";
            this.toolStripButton3.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "原始大小";
            this.toolStripButton4.Click += new System.EventHandler(this.实际大小ToolStripMenuItem_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "放大";
            this.toolStripButton5.Click += new System.EventHandler(this.放大ToolStripMenuItem_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "缩小";
            this.toolStripButton6.Click += new System.EventHandler(this.缩小ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "逆时针反转";
            this.toolStripButton7.Click += new System.EventHandler(this.逆时针旋转90度ToolStripMenuItem_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "顺时针反转";
            this.toolStripButton8.Click += new System.EventHandler(this.顺时针旋转90度ToolStripMenuItem_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "水平翻转";
            this.toolStripButton9.Click += new System.EventHandler(this.水平翻转ToolStripMenuItem_Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton10.Text = "垂直翻转";
            this.toolStripButton10.Click += new System.EventHandler(this.垂直翻转ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton11.Text = "反色";
            this.toolStripButton11.Click += new System.EventHandler(this.反色ToolStripMenuItem_Click);
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton12.Text = "锐化";
            this.toolStripButton12.Click += new System.EventHandler(this.锐化ToolStripMenuItem_Click);
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
            this.toolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton13.Text = "马赛克";
            this.toolStripButton13.Click += new System.EventHandler(this.马赛克效果ToolStripMenuItem_Click);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton14.Image")));
            this.toolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton14.Text = "灰度化";
            this.toolStripButton14.Click += new System.EventHandler(this.灰度化ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // singleColorChoose
            // 
            this.singleColorChoose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.singleColorChoose.Image = ((System.Drawing.Image)(resources.GetObject("singleColorChoose.Image")));
            this.singleColorChoose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.singleColorChoose.Name = "singleColorChoose";
            this.singleColorChoose.Size = new System.Drawing.Size(23, 22);
            this.singleColorChoose.Text = "纯色";
            this.singleColorChoose.Click += new System.EventHandler(this.纯色ToolStripMenuItem_Click);
            // 
            // color_show
            // 
            this.color_show.BackColor = System.Drawing.SystemColors.ControlDark;
            this.color_show.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.color_show.Enabled = false;
            this.color_show.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_show.Name = "color_show";
            this.color_show.Size = new System.Drawing.Size(23, 22);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // lineWidth
            // 
            this.lineWidth.Name = "lineWidth";
            this.lineWidth.Size = new System.Drawing.Size(44, 22);
            this.lineWidth.Text = "线宽：";
            // 
            // LineWidthtoolStripComboBox
            // 
            this.LineWidthtoolStripComboBox.Name = "LineWidthtoolStripComboBox";
            this.LineWidthtoolStripComboBox.Size = new System.Drawing.Size(75, 25);
            this.LineWidthtoolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.LineWidthtoolStripComboBox_SelectedIndexChanged);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // lineType
            // 
            this.lineType.Name = "lineType";
            this.lineType.Size = new System.Drawing.Size(44, 22);
            this.lineType.Text = "线型：";
            // 
            // LineType_toolStripDropDownButton1
            // 
            this.LineType_toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LineType_toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dssh_ToolStripMenuItem,
            this.dashDot_ToolStripMenuItem,
            this.dashDotDot_ToolStripMenuItem1,
            this.solid_ToolStripMenuItem1,
            this.dot_ToolStripMenuItem1});
            this.LineType_toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("LineType_toolStripDropDownButton1.Image")));
            this.LineType_toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LineType_toolStripDropDownButton1.Name = "LineType_toolStripDropDownButton1";
            this.LineType_toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.LineType_toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // dssh_ToolStripMenuItem
            // 
            this.dssh_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dssh_ToolStripMenuItem.Image")));
            this.dssh_ToolStripMenuItem.Name = "dssh_ToolStripMenuItem";
            this.dssh_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dssh_ToolStripMenuItem.Text = "Dash";
            this.dssh_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dashDot_ToolStripMenuItem
            // 
            this.dashDot_ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dashDot_ToolStripMenuItem.Image")));
            this.dashDot_ToolStripMenuItem.Name = "dashDot_ToolStripMenuItem";
            this.dashDot_ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.dashDot_ToolStripMenuItem.Text = "DashDot";
            this.dashDot_ToolStripMenuItem.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dashDotDot_ToolStripMenuItem1
            // 
            this.dashDotDot_ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("dashDotDot_ToolStripMenuItem1.Image")));
            this.dashDotDot_ToolStripMenuItem1.Name = "dashDotDot_ToolStripMenuItem1";
            this.dashDotDot_ToolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
            this.dashDotDot_ToolStripMenuItem1.Text = "DashDotDot";
            this.dashDotDot_ToolStripMenuItem1.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // solid_ToolStripMenuItem1
            // 
            this.solid_ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("solid_ToolStripMenuItem1.Image")));
            this.solid_ToolStripMenuItem1.Name = "solid_ToolStripMenuItem1";
            this.solid_ToolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
            this.solid_ToolStripMenuItem1.Text = "Solid";
            this.solid_ToolStripMenuItem1.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // dot_ToolStripMenuItem1
            // 
            this.dot_ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("dot_ToolStripMenuItem1.Image")));
            this.dot_ToolStripMenuItem1.Name = "dot_ToolStripMenuItem1";
            this.dot_ToolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
            this.dot_ToolStripMenuItem1.Text = "Dot";
            this.dot_ToolStripMenuItem1.Click += new System.EventHandler(this.LineStyleToolStripMenuItem_Click);
            // 
            // ToolStripLabel1
            // 
            this.ToolStripLabel1.Name = "ToolStripLabel1";
            this.ToolStripLabel1.Size = new System.Drawing.Size(44, 22);
            this.ToolStripLabel1.Text = "弧度：";
            // 
            // radianTextBox
            // 
            this.radianTextBox.Name = "radianTextBox";
            this.radianTextBox.Size = new System.Drawing.Size(50, 25);
            this.radianTextBox.TextChanged += new System.EventHandler(this.radianTextBox_TextChanged);
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chooseButton,
            this.line,
            this.curve,
            this.curvePlus,
            this.rectangle,
            this.solidRectangle,
            this.circle,
            this.solidCircle,
            this.polygon,
            this.solidPolygon,
            this.addText});
            this.toolStrip2.Location = new System.Drawing.Point(0, 50);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(44, 653);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // chooseButton
            // 
            this.chooseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.chooseButton.Image = ((System.Drawing.Image)(resources.GetObject("chooseButton.Image")));
            this.chooseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.chooseButton.Name = "chooseButton";
            this.chooseButton.Size = new System.Drawing.Size(42, 20);
            this.chooseButton.Text = "指针";
            this.chooseButton.Click += new System.EventHandler(this.chooseButton_Click);
            // 
            // line
            // 
            this.line.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.line.Image = ((System.Drawing.Image)(resources.GetObject("line.Image")));
            this.line.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(42, 20);
            this.line.Text = "直线";
            this.line.Click += new System.EventHandler(this.line_Click);
            // 
            // curve
            // 
            this.curve.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.curve.Image = ((System.Drawing.Image)(resources.GetObject("curve.Image")));
            this.curve.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.curve.Name = "curve";
            this.curve.Size = new System.Drawing.Size(42, 20);
            this.curve.Text = "曲线";
            this.curve.Click += new System.EventHandler(this.curve_Click);
            // 
            // curvePlus
            // 
            this.curvePlus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.curvePlus.Image = ((System.Drawing.Image)(resources.GetObject("curvePlus.Image")));
            this.curvePlus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.curvePlus.Name = "curvePlus";
            this.curvePlus.Size = new System.Drawing.Size(42, 20);
            this.curvePlus.Text = "弧线";
            this.curvePlus.Click += new System.EventHandler(this.curvePlus_Click);
            // 
            // rectangle
            // 
            this.rectangle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rectangle.Image = ((System.Drawing.Image)(resources.GetObject("rectangle.Image")));
            this.rectangle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rectangle.Name = "rectangle";
            this.rectangle.Size = new System.Drawing.Size(42, 20);
            this.rectangle.Text = "空矩形";
            this.rectangle.Click += new System.EventHandler(this.rectangle_Click);
            // 
            // solidRectangle
            // 
            this.solidRectangle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.solidRectangle.Image = ((System.Drawing.Image)(resources.GetObject("solidRectangle.Image")));
            this.solidRectangle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.solidRectangle.Name = "solidRectangle";
            this.solidRectangle.Size = new System.Drawing.Size(42, 20);
            this.solidRectangle.Text = "实矩形";
            this.solidRectangle.Click += new System.EventHandler(this.solidRectangle_Click);
            // 
            // circle
            // 
            this.circle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.circle.Image = ((System.Drawing.Image)(resources.GetObject("circle.Image")));
            this.circle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.circle.Name = "circle";
            this.circle.Size = new System.Drawing.Size(42, 20);
            this.circle.Text = "空圆";
            this.circle.Click += new System.EventHandler(this.circle_Click);
            // 
            // solidCircle
            // 
            this.solidCircle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.solidCircle.Image = ((System.Drawing.Image)(resources.GetObject("solidCircle.Image")));
            this.solidCircle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.solidCircle.Name = "solidCircle";
            this.solidCircle.Size = new System.Drawing.Size(42, 20);
            this.solidCircle.Text = "实心圆";
            this.solidCircle.Click += new System.EventHandler(this.solidCircle_Click);
            // 
            // polygon
            // 
            this.polygon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.polygon.Image = ((System.Drawing.Image)(resources.GetObject("polygon.Image")));
            this.polygon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.polygon.Name = "polygon";
            this.polygon.Size = new System.Drawing.Size(42, 20);
            this.polygon.Text = "空多边形";
            this.polygon.Click += new System.EventHandler(this.polygon_Click);
            // 
            // solidPolygon
            // 
            this.solidPolygon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.solidPolygon.Image = ((System.Drawing.Image)(resources.GetObject("solidPolygon.Image")));
            this.solidPolygon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.solidPolygon.Name = "solidPolygon";
            this.solidPolygon.Size = new System.Drawing.Size(42, 20);
            this.solidPolygon.Text = "实多边形";
            this.solidPolygon.Click += new System.EventHandler(this.solidPolygon_Click);
            // 
            // addText
            // 
            this.addText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addText.Image = ((System.Drawing.Image)(resources.GetObject("addText.Image")));
            this.addText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addText.Name = "addText";
            this.addText.Size = new System.Drawing.Size(42, 20);
            this.addText.Text = "文字";
            this.addText.Click += new System.EventHandler(this.addText_Click);
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.Location = new System.Drawing.Point(47, 53);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(862, 514);
            this.mainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mainPictureBox.TabIndex = 3;
            this.mainPictureBox.TabStop = false;
            this.mainPictureBox.SizeChanged += new System.EventHandler(this.mainPictureBox_SizeChanged);
            this.mainPictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mainPictureBox_MouseDoubleClick);
            this.mainPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mainPictureBox_MouseDown);
            this.mainPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mainPictureBox_MouseMove);
            this.mainPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mainPictureBox_MouseUp);
            // 
            // zxz_Main_Form
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(909, 703);
            this.Controls.Add(this.mainPictureBox);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.快捷按钮);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "zxz_Main_Form";
            this.Text = "图像编辑与绘制";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.zxz_Main_Form_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.zxz_Main_Form_DragEnter);
            this.Resize += new System.EventHandler(this.zxz_Main_Form_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.快捷按钮.ResumeLayout(false);
            this.快捷按钮.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新建ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 绘图设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 颜色设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 另存为ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 实际大小ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 放大ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩小ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 水平翻转ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 垂直翻转ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 顺时针旋转90度ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 逆时针旋转90度ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 反色ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 浮雕ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 黑白ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 柔化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 锐化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 灰度化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 雾化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 马赛克效果ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像大小设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 线型ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dssh__ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dash_Dot_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashDotDot_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solid_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 起点终点ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrowAnchorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diamondAnchorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem squareAnchorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem triangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roundAnchorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noanchorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 终点端点ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrowAnchorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem diamondAnchorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem squareAnchorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem triangleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem roundAnchorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem noanchorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 纯色ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 渐变色ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 纹理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片填充ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip 快捷按钮;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton singleColorChoose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel lineWidth;
        private System.Windows.Forms.ToolStripComboBox LineWidthtoolStripComboBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel lineType;
        private System.Windows.Forms.ToolStripButton chooseButton;
        private System.Windows.Forms.ToolStripButton line;
        private System.Windows.Forms.ToolStripButton curve;
        private System.Windows.Forms.ToolStripButton curvePlus;
        private System.Windows.Forms.ToolStripButton rectangle;
        private System.Windows.Forms.ToolStripButton solidRectangle;
        private System.Windows.Forms.ToolStripButton circle;
        private System.Windows.Forms.ToolStripButton solidCircle;
        private System.Windows.Forms.ToolStripButton polygon;
        private System.Windows.Forms.ToolStripButton solidPolygon;
        private System.Windows.Forms.ToolStripButton addText;
        private System.Windows.Forms.ToolStripButton color_show;
        private System.Windows.Forms.PictureBox mainPictureBox;
        private System.Windows.Forms.ToolStripDropDownButton LineType_toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem dssh_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashDot_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashDotDot_ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem solid_ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dot_ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripLabel ToolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox radianTextBox;

        public System.Windows.Forms.PictureBox MainPictureBox {
            get { return mainPictureBox; }
            set { mainPictureBox = value; }
        }
    }
}

