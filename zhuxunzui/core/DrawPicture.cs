﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
namespace zhuxunzui.core
{
    class DrawPicture {

        private ArrayList arrayPoint;

        public ArrayList ArrayPoint {
            get { return arrayPoint; }
            set { arrayPoint = value; }
        }

        //起点
        private Point startPoint;

        protected Point StartPoint {
            get { return startPoint; }
            set { startPoint = value; }
        }
        //终点
        private Point endPoint;

        protected Point EndPoint {
            get { return endPoint; }
            set { endPoint = value; }
        }
        //画笔
        private Brush brush;

        protected Brush Brush {
            get { return brush; }
            set { brush = value; }
        }

        private Pen pen;

        public Pen Pen {
            get { return pen; }
            set { pen = value; }
        }

        //编辑图片风格参数
        private DrawStyles drawStyles;

        internal DrawStyles DrawStyles {
            get { return drawStyles; }
            set { drawStyles = value; }
        }

        private Image img;

        public Image Img {
            get { return img; }
            set { img = value; }
        }

        public DrawPicture( DrawStyles ds) {
            drawStyles = ds;
            //将画笔初始化放在父类这里 这样 其他子类就省了初始化的繁琐
            this.pen = new Pen(this.DrawStyles.Color, this.DrawStyles.LineWidth);
            this.pen.DashStyle = this.DrawStyles.DashStyle;
            this.pen.StartCap = this.DrawStyles.StartLineCap;
            this.pen.EndCap = this.DrawStyles.EndLineCap;
        }

        public virtual void mouseClick(PictureBox pbx, MouseEventArgs e) {

        }

        public virtual void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {

        }

        public virtual bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return false;
        }

        public virtual void mouseMove(PictureBox pbx, MouseEventArgs e) {
            
        }

        public virtual void mouseDown(PictureBox pbx, MouseEventArgs e) {
            
        }

        protected Rectangle createReactangleByTwoPoint(Point a, Point b) {
            int minx = Math.Min(a.X, b.X);
            int miny = Math.Min(a.Y, b.Y);
            int width = Math.Abs(a.X-b.X);
            int height = Math.Abs(a.Y-b.Y);
            return new Rectangle(minx, miny, width, height);
        }

        
    }
}
