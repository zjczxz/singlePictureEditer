﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace zhuxunzui.core
{
    class ImgEffect {

        public enum EffectType{
            [System.ComponentModel.Description("浮雕")]
            Relief = 0,
            [System.ComponentModel.Description("黑白")]
            BlackWhite = 1,
            [System.ComponentModel.Description("柔化")]
            Soften = 2,
            [System.ComponentModel.Description("锐化")]
            Sharpening = 3,
            [System.ComponentModel.Description("灰度")]
            Gray = 4,
            [System.ComponentModel.Description("雾化")]
            Atomization = 5,
            [System.ComponentModel.Description("马赛克")]
            Mosaic = 6,
            [System.ComponentModel.Description("反色")]
            Reverse = 7
        }


        private static Bitmap oldImg;

        private static Bitmap newImg;

        private static bool checkCondition() {
            if (oldImg == null) {
                MessageBox.Show("未打开任何图片", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }


        /**
         * 为图片加特效 为了方便 想要用一个方法
         */ 
        public static Bitmap setEffect(Bitmap img, EffectType type) {
            oldImg = img;
            if (!checkCondition())
                return null;
            
            try {
                switch (type) {
                    case EffectType.Relief:
                        newImg = zhuxunzui.common.ToolUtils.relief(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    case EffectType.BlackWhite:
                        newImg = zhuxunzui.common.ToolUtils.blackWhite(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    case EffectType.Soften:
                        newImg = zhuxunzui.common.ToolUtils.soften(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    case EffectType.Sharpening:
                        newImg = zhuxunzui.common.ToolUtils.sharpening(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    case EffectType.Gray:
                        newImg = zhuxunzui.common.ToolUtils.gray(oldImg);
                        break;
                    case EffectType.Atomization:
                        newImg = zhuxunzui.common.ToolUtils.atomization(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    case EffectType.Mosaic:
                        newImg = zhuxunzui.common.ToolUtils.mosaic(oldImg, zhuxunzui.common.Const.MOSAICVALUE);
                        break;
                    case EffectType.Reverse:
                        newImg = zhuxunzui.common.ToolUtils.reverse(oldImg.Width, oldImg.Height, oldImg);
                        break;
                    default:
                        // do something here;
                        break;
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return newImg;
        }


    }
}
