﻿using System.Drawing;
using System.Windows.Forms;

namespace zhuxunzui.core.drawRelative
{
    class DrawArc : DrawPicture{
        public DrawArc(DrawStyles ds)
            : base(ds) {
        }

        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {

        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return true; // 鼠标抬起 表示结束绘制
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
                return;
            pbx.Image = (Bitmap)Img.Clone(); 
            Graphics g = Graphics.FromImage(pbx.Image);
            float start = 0, sweep = 0;
            Rectangle rec = common.CurveToolUitls.CalculationCurvePara(this.StartPoint, new Point(e.X, e.Y),
                this.DrawStyles.Radian,
                ref start,
                ref sweep
                );
            //画线填充
            this.Pen = GetBrush.createEffectPen(this.Pen,
                this.createReactangleByTwoPoint(this.StartPoint, new Point(e.X, e.Y)),
                this.DrawStyles);
            //画线填充结束
            if (rec.Width > 0 && rec.Height > 0) {
                g.DrawArc(this.Pen, rec, start, sweep);
            }
            //g.DrawLine(this.Pen, StartPoint, new Point(e.X, e.Y));


            g.Dispose();
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e) {
            this.Img = pbx.Image;
            this.StartPoint = new Point(e.X, e.Y);
        }


    }
}
