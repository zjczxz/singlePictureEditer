﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace zhuxunzui.core.drawRelative
{
    class DrawRectangle : DrawPicture {

        public DrawRectangle(DrawStyles ds)
            : base(ds) {
            
        }

        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {

        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return true;
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
                return;
            pbx.Image = (Bitmap)Img.Clone();
            Graphics g = Graphics.FromImage(pbx.Image);

            Rectangle rec = this.createReactangleByTwoPoint(new Point(e.X, e.Y), this.StartPoint);

            //画线填充
            this.Pen = GetBrush.createEffectPen(this.Pen,
                this.createReactangleByTwoPoint(this.StartPoint, new Point(e.X, e.Y)),
                this.DrawStyles);
            //画线填充结束

            g.DrawRectangle(this.Pen, rec);
            if (DrawStyles.DrawTypeF == core.DrawStyles.DrawType.SolidRectangle) {
                
                if(rec.Width > 0 && rec.Height > 0){
                    this.Brush = GetBrush.getBrushByDrawStyles(this.DrawStyles, rec);
                    g.FillRectangle(this.Brush, rec);
                }

            }


            g.Dispose();
            Console.WriteLine("鼠标移动");
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null) {
                this.Img = pbx.Image;
                this.StartPoint = new Point(e.X, e.Y);
            }
        }

    }
}
