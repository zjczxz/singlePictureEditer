﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;


namespace zhuxunzui.core.drawRelative
{
    class DrawCurve : DrawPicture
    {
        public DrawCurve(DrawStyles ds)
            : base(ds)
        {
            this.ArrayPoint = new ArrayList();
        }

        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e)
        {

        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e)
        {
            if (DrawStyles.DrawTypeF == core.DrawStyles.DrawType.Curve)
            {
                return true;
            }
            return false;
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e)
        {
            if (this.Img == null)
                return;

            //画线填充
            this.Pen = GetBrush.createEffectPen(this.Pen,
                this.createReactangleByTwoPoint(this.StartPoint, new Point(e.X, e.Y)),
                this.DrawStyles);
            //画线填充结束

            pbx.Image = (Bitmap)Img.Clone();
            Graphics g = Graphics.FromImage(pbx.Image);

            Point[] drawPoint = new Point[this.ArrayPoint.Count + 1];
            int i = 0;
            for (i = 0; i < this.ArrayPoint.Count; i++)
                drawPoint[i] = (Point)ArrayPoint[i];
            drawPoint[i] = new Point(e.X, e.Y);//记录当前坐标点
            if (DrawStyles.DrawTypeF == core.DrawStyles.DrawType.Curve)
            {
                this.ArrayPoint.Add(new Point(e.X, e.Y));
            }
            try
            {
                g.DrawCurve(this.Pen, drawPoint);
            }
            catch (Exception ex)
            {
                // dont konw why  
            }

            g.Dispose();
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e)
        {
            if (Img == null)
            {
                this.Img = pbx.Image;
                this.StartPoint = new Point(e.X, e.Y);
            }
            //画弧线 就在鼠标点击的时候 将点加入点集
            if (this.DrawStyles.DrawTypeF == core.DrawStyles.DrawType.CurvePlus)
            {
                this.ArrayPoint.Add(new Point(e.X, e.Y));
            }
        }


    }
}
