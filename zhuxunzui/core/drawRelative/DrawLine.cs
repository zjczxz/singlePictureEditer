﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace zhuxunzui.core.drawRelative
{
    class DrawLine : DrawPicture {

        public DrawLine(DrawStyles ds) : base(ds) {

        }

        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {

        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return true;
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
                return;
            pbx.Image = (Bitmap)Img.Clone(); 
            Graphics g = Graphics.FromImage(pbx.Image);
            //画线填充
            this.Pen = GetBrush.createEffectPen(this.Pen,
                this.createReactangleByTwoPoint(this.StartPoint, new Point(e.X, e.Y)),
                this.DrawStyles);
            //画线填充结束
            g.DrawLine(this.Pen, this.StartPoint, new Point(e.X, e.Y));
            g.Dispose();
            Console.WriteLine("鼠标移动");
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e) {
            this.Img = pbx.Image;
            this.StartPoint = new Point(e.X, e.Y);
        }
    }
}
