﻿using System.Collections;
using System.Drawing;
using System.Windows.Forms;


namespace zhuxunzui.core.drawRelative
{
    class DrawString : DrawPicture
    {
        public DrawString(DrawStyles ds)
            : base(ds) {
            this.ArrayPoint = new ArrayList();
        }


        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {
        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return true;
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
                return;
            pbx.Image = (Bitmap)Img.Clone();
            Graphics g = Graphics.FromImage(pbx.Image);
            Rectangle rec = this.createReactangleByTwoPoint(new Point(e.X, e.Y), this.StartPoint);

            if (rec.Height > 0 && rec.Width > 0) {
                this.Brush = GetBrush.getBrushByDrawStyles(this.DrawStyles,
                    rec);
                g.DrawString(this.DrawStyles.DrawString, this.DrawStyles.Font,
                    this.Brush, rec);
            }
            g.Dispose();
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e) {

            if (Img == null)
                this.Img = pbx.Image;
            StartPoint = new Point(e.X, e.Y);
        }

        public override void mouseClick(PictureBox pbx, MouseEventArgs e) {
        }

    }
}
