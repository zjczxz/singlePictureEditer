﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;


namespace zhuxunzui.core.drawRelative
{
    class DrawPolygon : DrawPicture
    {


        public DrawPolygon(DrawStyles ds)
            : base(ds) {
            this.ArrayPoint = new ArrayList();
        }


        public override void mouseDoubleClick(PictureBox pbx, MouseEventArgs e) {
        }

        public override bool mouseUp(PictureBox pbx, MouseEventArgs e) {
            return false;
        }

        public override void mouseMove(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
                return;
            pbx.Image = (Bitmap)Img.Clone();
            Graphics g = Graphics.FromImage(pbx.Image);
            Point[] drawPoint = new Point[this.ArrayPoint.Count + 1];
            for (int i = 0; i < this.ArrayPoint.Count; i++) {
                drawPoint[i] = (Point)ArrayPoint[i];
            }

            drawPoint[this.ArrayPoint.Count] = new Point(e.X, e.Y);

            if (DrawStyles.DrawTypeF == core.DrawStyles.DrawType.SolidPolygon) {
                int minx = 1000000000, miny = 1000000000, maxx = 0, maxy = 0;

                for (int i = 0; i < this.ArrayPoint.Count; i++) {
                    drawPoint[i] = (Point)ArrayPoint[i];
                    minx = Math.Min(minx, drawPoint[i].X);
                    miny = Math.Min(miny, drawPoint[i].Y);
                    maxx = Math.Max(maxx, drawPoint[i].X);
                    maxy = Math.Max(maxy, drawPoint[i].Y);
                }
                
                if (this.ArrayPoint.Count > 1) {
                    if (maxy - miny > 0 && maxx - minx > 0) {
                        this.Brush = GetBrush.getBrushByDrawStyles(this.DrawStyles, new Rectangle(minx, miny, maxx - minx, maxy - miny));
                        g.FillPolygon(Brush, drawPoint);
                    }
                }

            }
            //画线填充
            this.Pen = GetBrush.createEffectPen(this.Pen,
                this.createReactangleByTwoPoint(this.StartPoint, new Point(e.X, e.Y)),
                this.DrawStyles);
            //画线填充结束
            g.DrawPolygon(this.Pen, drawPoint);
            g.Dispose();
        }

        public override void mouseDown(PictureBox pbx, MouseEventArgs e) {
            if (this.Img == null)
            {
                this.Img = pbx.Image;
                this.StartPoint = new Point(e.X, e.Y);
            }
            this.ArrayPoint.Add(new Point(e.X, e.Y));
        }

        public override void mouseClick(PictureBox pbx, MouseEventArgs e) {
            if (Img == null)
                this.Img = pbx.Image;

            this.ArrayPoint.Add(new Point(e.X, e.Y));
        }
    }
}
