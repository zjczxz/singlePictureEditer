﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

//using zhuxunzui.common;

namespace zhuxunzui.core
{
    class imgControler
    {
        private bool modify;
        private String fullPath;
        private PictureBox pictureBox;




        public enum ConvertType
        {
            [System.ComponentModel.Description("水平反转")]
            Rotate180FlipY = 0,
            [System.ComponentModel.Description("垂直反转")]
            Rotate180FlipX = 1,
            [System.ComponentModel.Description("顺时针旋转90度")]
            Rotate90FlipNone = 2,
            [System.ComponentModel.Description("逆时针旋转90度")]
            Rotate270FlipNone = 3 //也就是顺时针270度。
        }




        public String FullPath {
            get { return fullPath; }
            set { fullPath = value; }
        }

        public bool Modify {
            get { return modify; }
            set { modify = value; }
        }

        public imgControler(PictureBox pb)
        {
            this.pictureBox = pb;
            this.modify = false;
        }

        private bool checkCondition()
        {
            if (pictureBox.Image == null)
            {
                MessageBox.Show("未打开任何图片", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        /*
         * 直接打开图片 利用打开窗口在文件中选择。 imgconsole类方法
         */
        public bool openImg()
        {
            if (!checkModify())
            { // 图片是否保存 为保存 提示保存 除非点取消否则操作失效
                return false;
            }
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "bmp,jpg,gif,png,tiff,icon|*.bmp;*.jpg;*.gif;*.png;*.tiff;*.icon";
            dialog.Title = "选择图片";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                drawImageByFullFileName(dialog.FileName.ToString());

                return true;
            }
            return false;
        }

        private void drawImageByFullFileName(String fileName)
        {
            Bitmap tempImage = new Bitmap(fileName);
            Bitmap img = new Bitmap(tempImage.Width, tempImage.Height);
            Graphics draw = Graphics.FromImage(img);
            draw.DrawImage(tempImage, 0, 0, tempImage.Width, tempImage.Height);
            this.fullPath = fileName;
            pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox.Image = img;
            draw.Dispose();
            tempImage.Dispose();
        }

        /*
         * 使用fullPath打开图片 
         * 大部分用于拖入 或 系统打开方式 也可能在 构造函数中实现
         */
        public bool openImg(String fullPath)
        {
            if (!checkModify())
            { // 图片是否保存 为保存 提示保存 除非点取消否则操作失效
                return false;
            }
            drawImageByFullFileName(fullPath);
            this.modify = false;
            return true;
        }

        /*
         * 新建图片 在新建前检查当前是否有图片 TODO当前图片是否保存等问题。
         */
        public bool newImg()
        {

            if (!checkModify())
            { // 图片是否保存 为保存 提示保存 除非点取消否则操作失效
                return false;
            }
            this.modify = false;
            Bitmap newimg = new Bitmap(pictureBox.Width, pictureBox.Height);
            Graphics g = Graphics.FromImage(newimg);
            SolidBrush sb = new SolidBrush(Color.White);
            g.FillRectangle(sb, 0, 0, pictureBox.Width, pictureBox.Height);
            pictureBox.Image = newimg;

            return true;

            //return false;
        }

        private bool checkModify()
        {

            if (this.modify)
            {
                DialogResult res = MessageBox.Show("图片已更改，是否保存", "警告",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (res == DialogResult.OK)
                {
                    if (!this.saveImg()) // 保存成功
                        return false;
                }

                if (res == DialogResult.Cancel)
                { //点取消表示未保存
                    return false;
                }
            }
            return true;
        }

        /**
         * 保存图片
         * 失败返回 false。 成功返回true
         * 成功会清空修改标志
         */
        public bool saveImg()
        {
            if (this.fullPath == null || this.fullPath.Equals(""))
            {//如果是第一次保存
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Jpeg图片|*.jpg|位图文件|*.bmp|Gif动图|*.gif|png|*.png|tiff|*.tiff|icon|*.icon";
                sfd.OverwritePrompt = true;
                sfd.Title = "保存图像";
                sfd.ValidateNames = true;
                sfd.RestoreDirectory = true;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    this.fullPath = sfd.FileName;
                    switch (sfd.FilterIndex)
                    {
                        case 1: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Jpeg); break; }
                        case 2: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Bmp); break; }
                        case 3: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Gif); break; }
                        case 4: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Png); break; }
                        case 5: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Tiff); break; }
                        case 6: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Icon); break; }

                    }
                    this.modify = false; // 保存后 清空修改标志
                    return true;
                }
            }
            else
            {//文件已经存在
                this.pictureBox.Image.Save(fullPath);//直接保存
                this.modify = false;    // 保存后 清空修改标志
                return true;
            }

            return false;
        }

        /**
         * 将图片另存为
         * 比如可以另存为其他格式 其他文件夹
         * 另存成功后将当前文件的路径设置到新的路径+名字.类型
         */
        public bool saveAs()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Jpeg图片|*.jpg|位图文件|*.bmp|Gif动图|*.gif|png|*.png|tiff|*.tiff|icon|*.icon";
            sfd.OverwritePrompt = true;
            sfd.Title = "另存为图像";
            sfd.ValidateNames = true;
            sfd.RestoreDirectory = true;
            sfd.InitialDirectory = "C:\\";//初始化目录
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                switch (sfd.FilterIndex)
                {
                    case 1: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Jpeg); break; }
                    case 2: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Bmp); break; }
                    case 3: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Gif); break; }
                    case 4: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Png); break; }
                    case 5: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Tiff); break; }
                    case 6: { this.pictureBox.Image.Save(sfd.FileName, ImageFormat.Icon); break; }

                }
                fullPath = sfd.FileName;
                this.modify = false; // 清空 修改标志
                return true;
            }

            return false;
        }
        //缩小
        public void enlarge()
        {
            this.changeSizeByRatio(zhuxunzui.common.Const.ENLARGERATIO);
        }
        //放大
        public void reduce()
        {
            this.changeSizeByRatio(zhuxunzui.common.Const.REDUCERATIO);
        }

        //按比例缩放图片的大小
        public void changeSizeByRatio(double r)
        {
            pictureBox.Height = (int)Math.Ceiling(pictureBox.Height * r);
            pictureBox.Width = (int)Math.Ceiling(pictureBox.Width * r);
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
        }

        public void orignSize()
        {
            pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
        }




        /**
         * 图片翻转
         * 
         */
        public void convertImage(ConvertType type)
        {
            if (!checkCondition())
                return;
            this.modify = true;
            Bitmap curimg = (Bitmap)pictureBox.Image;
            switch (type)
            {
                case ConvertType.Rotate180FlipY:
                    {//水平旋转
                        curimg.RotateFlip(RotateFlipType.Rotate180FlipY);
                        break;
                    }
                case ConvertType.Rotate180FlipX:
                    { //垂直翻转
                        curimg.RotateFlip(RotateFlipType.Rotate180FlipX);
                        break;
                    }
                case ConvertType.Rotate90FlipNone:
                    { //顺时针翻转90读
                        curimg.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    }
                case ConvertType.Rotate270FlipNone:
                    { //逆时针翻转90度
                        curimg.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                    }
            }

            Bitmap tempImage = null;
            if (type == ConvertType.Rotate270FlipNone || type == ConvertType.Rotate90FlipNone)
                tempImage = new Bitmap(pictureBox.Image.Width, pictureBox.Image.Height);
            else
                tempImage = new Bitmap(pictureBox.Image.Height, pictureBox.Image.Width);

            Graphics draw = Graphics.FromImage(tempImage);
            draw.DrawImage(curimg, 0, 0, tempImage.Width, tempImage.Height);
            pictureBox.Image = tempImage;
            pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
        }



        //private Point originPosition;
        //private Point clickPosition;
        /*
         * 图片移动相关 与 鼠标按下 移动 抬起有关
         * */
        //public void mouseDown( MouseEventArgs e)
        //{
        //    originPosition = pictureBox.Location;
        //    clickPosition = new Point(e.X, e.Y);
        //}

        //public void mouseMove( MouseEventArgs e)
        //{
        //    if (originPosition == null || originPosition.X == -999999)
        //        return;

        //    int xMove = e.X - clickPosition.X;
        //    int yMove = e.Y - clickPosition.Y;
        //    clickPosition = new Point(e.X, e.Y);
        //    Console.WriteLine("{0} {1} {2} {3}", e.X, e.Y, clickPosition.X,clickPosition.Y);
        //    //pictureBox.Location = new Point(originPosition.X + xMove, originPosition.Y + yMove);
        //    pictureBox.Left += xMove;
            
        //}

        //public void mouseUp( MouseEventArgs e)
        //{
        //    originPosition = new Point(-999999, -999999);
        //}

    }
}
