﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace zhuxunzui.core
{
    class GradientColorConfig{
        Color startColor;

        public GradientColorConfig() {
            this.Dir = LinearGradientMode.Horizontal;
            this.StartColor = Color.Red;
            this.EndColor = Color.Black;
        }

        public Color StartColor {
            get { return startColor; }
            set { startColor = value; }
        }
        Color endColor;

        public Color EndColor {
            get { return endColor; }
            set { endColor = value; }
        }
        LinearGradientMode dir;

        public LinearGradientMode Dir {
            get { return dir; }
            set { dir = value; }
        }
    }

    class HashConfig
    {
        public HashConfig() {
            this.BackColor = Color.Blue;
            this.frontColor = Color.Yellow;
            this.hashStyle = HatchStyle.BackwardDiagonal;
        }
        private HatchStyle hashStyle;

        public HatchStyle HashStyle {
            get { return hashStyle; }
            set { hashStyle = value; }
        }
        Color backColor;

        public Color BackColor {
            get { return backColor; }
            set { backColor = value; }
        }
        Color frontColor;

        public Color FrontColor {
            get { return frontColor; }
            set { frontColor = value; }
        }
    }

    class DrawStyles {
        // ColorType
        //颜色或填充颜色类型
        public enum ColorType {
            [System.ComponentModel.Description("纯色")]
            SingleColor = 0,
            [System.ComponentModel.Description("渐变色")]
            GradientColor = 1,
            [System.ComponentModel.Description("纹理")]
            Texture = 2,
            [System.ComponentModel.Description("图片填充")]
            PictureFill = 3,
        }

        private GradientColorConfig gradientConfig;

        private HashConfig hashConfig;

        internal HashConfig HashConfig {
            get { return hashConfig; }
            set { hashConfig = value; }
        }

        private Image fillImg;

        public Image FillImg {
            get { return fillImg; }
            set { fillImg = value; }
        }

        private String drawString;

        public String DrawString {
            get { return drawString; }
            set { drawString = value; }
        }

        private float radian;


        public DrawStyles() {

            //对填充渐变色做初始化
            gradientConfig = new GradientColorConfig();

            hashConfig = new HashConfig();

            fillImg = new Bitmap(50, 50);

            colorType = ColorType.SingleColor;

            color = Color.Red;

            font = new Font("宋体", (float)15.75);

            drawString = "";

            dashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            radian = 90;
            startLineCap = LineCap.NoAnchor;
            endLineCap = LineCap.NoAnchor;

        }

        internal GradientColorConfig GradientConfig {
            get { return gradientConfig; }
            set { gradientConfig = value; }
        }

        /**
         * 浪费了很多时间来设计这些枚举， 后面没用到。 只能忍痛注释
         * 
         * */
        //线类型
        //public enum LineType {
        //    [System.ComponentModel.Description("虚线")]
        //    Dash = 0,
        //    [System.ComponentModel.Description("虚线+点")]
        //    DashDot = 1,
        //    [System.ComponentModel.Description("虚线+点+点")]
        //    DashDotDot = 2,
        //    [System.ComponentModel.Description("实线")]
        //    Solid = 3,
        //    [System.ComponentModel.Description("点")]
        //    Dot = 4
        //}

        //端点类型
        //public enum AnchorType {
        //    [System.ComponentModel.Description("没有端点")]
        //    NoAnchor = 0,
        //    [System.ComponentModel.Description("。。。")]
        //    ArrowAnchor = 1,
        //    [System.ComponentModel.Description("...")]
        //    DiamondAnchor = 2,
        //    [System.ComponentModel.Description("...")]
        //    SquareAnchor = 3,
        //    [System.ComponentModel.Description("...")]
        //    Triangle = 4,
        //    [System.ComponentModel.Description("...")]
        //    RoundAnchor = 5
        //}

        /**
         * 画的类型：比如直线 曲线等
         * 
         * */
        public enum DrawType {
            [System.ComponentModel.Description("直线")]
            SingleLine = 0,
            [System.ComponentModel.Description("曲线")]
            Curve = 1,
            [System.ComponentModel.Description("弧线")]
            CurvePlus = 2,
            [System.ComponentModel.Description("空心矩形")]
            Rectangle = 3,
            [System.ComponentModel.Description("实心矩形")]
            SolidRectangle = 4,
            [System.ComponentModel.Description("空心椭圆")]
            Circle = 5,
            [System.ComponentModel.Description("实心椭圆")]
            SolidCircle = 6,
            [System.ComponentModel.Description("空心多边形")]
            Polygon = 7,
            [System.ComponentModel.Description("实心多边形")]
            SolidPolygon = 8,
            [System.ComponentModel.Description("添加文本")]
            AddText = 9
        }

        private ColorType colorType;

        private DrawType drawType;

        //线的颜色 只有在纯色的时候出现 
        private Color color;

        private DashStyle dashStyle;

        private LineCap startLineCap;

        private LineCap endLineCap;

        private int lineWidth;

        public int LineWidth {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        private Font font;

        public Font Font {
            get { return font; }
            set { font = value; }
        }


        /**
         * 一些属性做到后面再加  比如 渐变色 填充图片路劲等 前面用不到的 先不加没有影响
         * 
         * */



        public LineCap StartLineCap {
            get { return startLineCap; }
            set { startLineCap = value; }
        }

        public LineCap EndLineCap {
            get { return endLineCap; }
            set { endLineCap = value; }
        }
        

        public Color Color {
            get { return color; }
            set { color = value; }
        }

        public DashStyle DashStyle {
            get { return dashStyle; }
            set { dashStyle = value; }
        }


        internal DrawType DrawTypeF {
            get { return drawType; }
            set { drawType = value; }
        }

        internal ColorType ColorTypeF {
            get { return colorType; }
            set { colorType = value; }
        }

        public float Radian { get => radian; set => radian = value; }
    }
}
