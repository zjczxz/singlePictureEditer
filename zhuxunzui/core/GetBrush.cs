﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace zhuxunzui.core
{
    class GetBrush
    {
        public static Brush getBrushByDrawStyles(DrawStyles ds, Rectangle rec) {
            if (ds.ColorTypeF == DrawStyles.ColorType.GradientColor) {
                return new LinearGradientBrush(
                rec,
                ds.GradientConfig.StartColor,
                ds.GradientConfig.EndColor,
                ds.GradientConfig.Dir);
            }
            else if (ds.ColorTypeF == DrawStyles.ColorType.Texture) {
                return new HatchBrush(ds.HashConfig.HashStyle,
                ds.HashConfig.FrontColor,
                ds.HashConfig.BackColor);
            }
            else if (ds.ColorTypeF == DrawStyles.ColorType.PictureFill) {
                return new TextureBrush(ds.FillImg);
            }else {
                return new SolidBrush(ds.Color);
            }
        }


        public static Pen createEffectPen(Pen pen, Rectangle rec, DrawStyles ds) {
            if (rec.Height > 0 && rec.Width > 0) {
                Brush tempBrush = GetBrush.getBrushByDrawStyles(ds, rec);
                Pen tempPen = new Pen(tempBrush);
                tempPen.Width = ds.LineWidth;
                tempPen.DashStyle = ds.DashStyle;
                tempPen.StartCap = ds.StartLineCap;
                tempPen.EndCap = ds.EndLineCap;
                return tempPen;
            } else
                return pen;
        }
    }
}
