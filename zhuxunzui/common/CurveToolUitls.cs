﻿using System;
using System.Drawing;
namespace zhuxunzui.common
{
    class CurveToolUitls
    {
        private static double SQRT2 = Math.Sqrt(2);

        private static double distinctPointPoint(Point a, Point b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }

        public static Rectangle CalculationCurvePara(Point a, Point b,
            float arc, ref float s, ref float sweep)
        {
            Point[] center = new Point[2];
            Point resultCenter;
            //求圆心 有两个。
            double radius = distinctPointPoint(a, b) / 2.0 / Math.Sin(arc / 2.0 / 180 * Math.PI);

            Circle_Center(a, b, radius, ref center[0], ref center[1]);
            if (a.X < b.X)
            {
                if (a.Y < b.Y)
                { // a左上 
                    resultCenter = center[0].X < center[1].X ? center[0] : center[1];
                    double t2 = Math.Asin(Math.Abs(resultCenter.Y - a.Y) * 1.0 / radius) / Math.PI * 180;
                    //double t1 = Math.Asin(Math.Abs(resultCenter.Y - b.Y) * 1.0 / radius) / Math.PI * 180;

                    if (resultCenter.X > a.X)
                    {
                        s = (float)(180 + t2);
                    }
                    else
                    {
                        s = (float)(360 - t2);

                    }
                    double t1 = Math.Asin(Math.Abs(resultCenter.Y - b.Y) * 1.0 / radius) / Math.PI * 180;

                    if (resultCenter.Y < b.Y) // 圆心在上面
                    {
                        sweep = (float)(360 - s + t1);
                    }
                    else
                    {
                        sweep = (float)(360 - s - t1);
                    }

                }
                else
                { //a 左下
                    resultCenter = center[0].X > center[1].X ? center[0] : center[1];
                    double t2 = Math.Asin(Math.Abs(resultCenter.Y - a.Y) * 1.0 / radius) / Math.PI * 180;
                    if (resultCenter.Y > a.Y)
                    {
                        s = (float)(180 + t2);
                    }
                    else
                    {
                        s = (float)(180 - t2);

                    }
                    double t1 = Math.Asin(Math.Abs(resultCenter.Y - b.Y) * 1.0 / radius) / Math.PI * 180;

                    if (resultCenter.X < b.X) // 圆心在b的左侧
                    {
                        sweep = (float)(360 - t1 - s);
                    }
                    else
                    {
                        sweep = (float)(180 +　t1 - s);
                    }

                }
            }
            else
            {
                if (a.Y < b.Y)
                {// a右shang

                    resultCenter = center[0].X > center[1].X ? center[0] : center[1];

                    double t2 = Math.Asin(Math.Abs(resultCenter.Y - b.Y) * 1.0 / radius) / Math.PI * 180;
                    if (resultCenter.Y > b.Y)
                    {
                        s = (float)(180 + t2);
                    }
                    else
                    {
                        s = (float)(180 - t2);

                    }
                    double t1 = Math.Asin(Math.Abs(resultCenter.Y - a.Y) * 1.0 / radius) / Math.PI * 180;

                    if (resultCenter.X < a.X) // 圆心在b的左侧
                    {
                        sweep = (float)(360 - t1 - s);
                    }
                    else
                    {
                        sweep = (float)(180 + t1 - s);
                    }


                }
                else
                { // a右xia
                    resultCenter = center[0].Y > center[1].Y ? center[0] : center[1];
                    double t2 = Math.Asin(Math.Abs(resultCenter.Y - b.Y) * 1.0 / radius) / Math.PI * 180;
                    if (resultCenter.X > b.X)
                    {
                        s = (float)(180 + t2);
                    }
                    else
                    {
                        s = (float)(360 - t2);

                    }

                    double t1 = Math.Asin(Math.Abs(resultCenter.Y - a.Y) * 1.0 / radius) / Math.PI * 180;

                    if (resultCenter.Y < a.Y) // 圆心在上面
                    {
                        sweep = (float)(360 - s + t1);
                    }
                    else
                    {
                        sweep = (float)(360 - s - t1);
                    }

                }
            }

            return new Rectangle(resultCenter.X - (int)radius, resultCenter.Y - (int)radius,
                 (int)radius * 2, (int)radius * 2);
        }

        //已知两个点 和圆半径 就圆心 解方程思路
        private static void Circle_Center(Point p1, Point p2, double dRadius, ref Point center1, ref Point center2)
        {
            double k = 0.0, k_verticle = 0.0;
            double mid_x = 0.0, mid_y = 0.0;
            double a = 1.0;
            double b = 1.0;
            double c = 1.0;
            center1 = new Point();
            center2 = new Point();
            k = (p2.Y - p1.Y) * 1.0 / (p2.X - p1.X);
            if (Math.Abs(k) < 0.00000001) 
            {
                center1.X = (int)((p1.X + p2.X) / 2.0);
                center2.X = (int)((p1.X + p2.X) / 2.0);
                center1.Y = p1.Y + (int)Math.Sqrt(dRadius * dRadius - (p1.X - p2.X) * (p1.X - p2.X) / 4.0);
                center2.Y = p2.Y - (int)Math.Sqrt(dRadius * dRadius - (p1.X - p2.X) * (p1.X - p2.X) / 4.0);
            }
            else
            {
                k_verticle = -1.0 / k;
                mid_x = (p1.X + p2.X) / 2.0;
                mid_y = (p1.Y + p2.Y) / 2.0; // 求中点 中垂线？
                a = 1.0 + k_verticle * k_verticle;
                b = -2 * mid_x - k_verticle * k_verticle * (p1.X + p2.X);
                c = mid_x * mid_x + k_verticle * k_verticle * (p1.X + p2.X) * (p1.X + p2.X) / 4.0 -
                    (dRadius * dRadius - ((mid_x - p1.X) * (mid_x - p1.X) + (mid_y - p1.Y) * (mid_y - p1.Y)));

                double x1, x2;
                x1 = ((-1.0 * b + Math.Sqrt(Math.Abs(b * b - 4 * a * c))) / (2 * a));
                x2 = ((-1.0 * b - Math.Sqrt(Math.Abs(b * b - 4 * a * c))) / (2 * a));
                center1.X = (int)x1;
                center2.X = (int)x2;
                center1.Y = (int)Y_Coordinates(mid_x, mid_y, k_verticle, x1);
                center2.Y = (int)Y_Coordinates(mid_x, mid_y, k_verticle, x2);
            }

        }

        private static double Y_Coordinates(double x, double y, double k, double x0)
        {
            return k * x0 - k * x + y;
        }
    }
}
