﻿/**
 * 用于项目使用的常数设置 利用xml文件配置成本较大，直接用类算了。
 */
namespace zhuxunzui.common
{
    class Const
    {
        public static double ENLARGERATIO = 1.1;
        public static double REDUCERATIO = 0.9;


        public static int MOSAICVALUE = 6;

        public static double POLYGONPOINTDISTINCT = 20.2;

        public static int PicBoxOriginPositionX = 0;

        public static int PicBoxOriginPositionY = 0;
    }
}
